# API EXAMPLE

This is a repository of know how generate an Api documentation whit `Blueprint` and `aglio` for more information you can review [Blueprint](https://apiblueprint.org), [Aglio](https://github.com/danielgtaylor/aglio).

## What do you need?

For this project you need have install:

* Npn version 10.6.0
* Nvm(optional)

Tools for editors:

* https://apiblueprint.org/tools.html

## How install dependences?

For install all dependences for this project you only need run:

```bash
$ npm install
```

## How generate html file?

```bash
$ aglio -i input.apib -o output.html
```

For more information you can consult:

* https://github.com/danielgtaylor/aglio#executable

